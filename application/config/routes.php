<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['users/(:num)'] = 'users/operate/$1';
$route['users/(add)'] = 'users/operate/$1';
$route['users'] = 'users/index';

$route['products/(:num)'] = 'products/operate/$1';
$route['products/(add)'] = 'products/operate/$1';
$route['products'] = 'products/index';

$route['category/(:num)'] = 'category/operate/$1';
$route['category/(add)'] = 'category/operate/$1';
$route['category'] = 'category/index';

$route['attributes/(:num)'] = 'attributes/operate/$1';
$route['attributes/(add)'] = 'attributes/operate/$1';
$route['attributes'] = 'attributes/index';

$route['attributegroup/(:num)'] = 'attributegroup/operate/$1';
$route['attributegroup/(add)'] = 'attributegroup/operate/$1';
$route['attributegroup'] = 'attributegroup/index';

$route['image'] = 'image/index';
$route['image/do_upload'] = 'image/do_upload';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['ajax-image-upload'] = 'AjaxImageUpload';
$route['ajax-image-upload/post']['post'] = "AjaxImageUpload/uploadImage";

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/

$route['user/get'] = 'api/user_api/user/get';
$route['user/signup'] = 'api/user_api/user/post';
$route['user/modify'] = 'api/user_api/user/put';
$route['user/login'] = 'api/user_api/login/post';
$route['user/logout'] = 'api/user_api/logout/get';

$route['category/get'] = 'api/category_api/category/get';
$route['categories/get'] = 'api/category_api/categories/get';

$route['attributes/get'] = 'api/attribute_api/atgs/get';
$route['attribute/get'] = 'api/attribute_api/atg/get';
$route['values/get'] = 'api/attribute_api/atbs/get';
$route['value/get'] = 'api/attribute_api/atb/get';




$route['products/get'] = 'api/product_api/products/get';
$route['product/get'] = 'api/product_api/product/get';
$route['product/save'] = 'api/product_api/productsave/post';
$route['product/down'] = 'api/product_api/productsave/get';
$route['products/home'] = 'api/product_api/productsHome/get';


$route['discovery/start'] = 'api/discovery_api/discovery_start/get';
$route['discovery/swipe'] = 'api/discovery_api/swipe/get';
$route['discovery/end'] = 'api/discovery_api/discovery_end/get';

$route['api/example/users/(:num)'] = 'api/example/users/id/$1';
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4';
