<?php
class Users extends CI_Controller {

	public function index(){
		$data['title'] = 'User';
		$data['users'] = $this->User_model->getAllUser();
		$this->load->view('templates/header');
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	}

	public function operate($id){
		$data = array();
		if($id != 'add'){
			$data['user'] = $this->User_model->getAllUser($id);
			if(empty($data['user'])){
				show_404();
			}
			$data['user_id'] = $data['user']['user_id'];
			$data['user_name'] = $data['user']['user_name'];
			$data['user_email'] = $data['user']['user_email'];
		}
		$this->load->view('templates/header');
		$this->load->view('users/operate', $data);
		$this->load->view('templates/footer');
	}
	public function remove($id){
		$this->User_model->deleteUser($id);
		redirect('/users');
	}
}
