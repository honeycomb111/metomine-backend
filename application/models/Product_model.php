<?php

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Product"))
 */

class Product_model extends CI_Model {
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pdt_id;
    /**
     * @SWG\Property()
     * @var string
     */
    public $pdt_name;
    /**
     * @var string
     * @SWG\Property()
     */
    public $pdt_description;
    /**
     * @var float
     * @SWG\Property()
     */
    public $pdt_price;
    /**
     * @var string
     * @SWG\Property()
     */
    public $access_token;
    
    public function getAllProduct($product_id = 0) {
        $sql = "SELECT * FROM `product` LEFT JOIN product_to_category as ptcr on product.pdt_id = ptcr.ptc_id LEFT JOIN category on ptcr.ptc_category_id = category.ctg_id";
        if($product_id > 0){
            $sql .= " WHERE product.pdt_id = {$product_id} ";
        }/*else{
            $sql = "SELECT * FROM product WHERE pdt_id > 0 ";
        }*/

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return ($product_id == 0) ? $result : $result[0];
    }

    public function getProductAttribute($product_id = 0) {
        $sql = "SELECT * FROM product_to_attribute ";
        if($product_id > 0){
            $sql = " WHERE pdt_id={$product_id}";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    
    public function getProduct($product_id) {
        $sql = "SELECT * FROM product WHERE pdt_id={$product_id}";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : null;
    }

    public function getProducts($filter_data) 
    {
        $sql = "SELECT DISTINCT product.* FROM product LEFT JOIN product_to_category ON pdt_id=ptc_id LEFT JOIN product_to_attribute ON pdt_id=pta_id ";

        $this->load->helper('MY_string_helper');

        // product
        if (isset($filter_data['pdt_id'])) {
            $sql =  sql_where($sql, "pdt_id=".$filter_data['pdt_id']);
        
        }

        if (isset($filter_data['pdt_name'])) {
            $sql =  sql_where($sql, "pdt_name LIKE '%" . $filter_data['pdt_name'] ."%'");
        }

        // Category 
        if (isset($filter_data['category'])) {
            $sql =  sql_where($sql, "ptc_category_id=".$filter_data['category']);
        }
        
        // Attribute
        if (isset($filter_data['attribute'])) {

            
            foreach ($filter_data['attribute'] as $attribute) {
                $attribute_string = implode(",", $attribute);
                $sql = sql_where($sql, "pta_attribute_id IN (" . $attribute_string .")");
            }
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function addProduct($product_data) {
        $sql = "INSERT INTO product (pdt_name, pdt_description, pdt_price) VALUES ('{$product_data['pdt_name']}', '{$product_data['pdt_description']}', {$product_data['pdt_price']})";
        $this->db->query($sql);
        $pdt_id = $this->db->insert_id();

        if ($product_data['category']) {
            $sql = "INSERT INTO product_to_category (ptc_id, ptc_category_id) VALUES({$pdt_id},{$product_data['category']})";
            $this->db->query($sql);
        }

        if ($product_data['attributes']) {
            foreach($product_data['attributes'] as $attribute) {
                $sql = "INSERT INTO product_to_attribute (pta_id, pta_attribute_id) VALUES({$pdt_id},{$attribute})";
                $this->db->query($sql);
            }
        }
    }


    public function updateProduct($product_id, $product_data) {
        $sql = "UPDATE  product SET pdt_name='{$product_data['pdt_name']}', pdt_description='{$product_data['pdt_description']}', pdt_price='{$product_data['pdt_price']}' WHERE pdt_id={$product_id}";
        $this->db->query($sql);

        if ($product_data['category']) {
            $sql = "UPDATE  product_to_category SET ptc_category_id={$product_data['category']} WHERE ptc_id={$product_id}";
            $this->db->query($sql);
        }

        if ($product_data['attributes']) {
            $sql = "DELETE FROM product_to_attribute WHERE pta_id={$product_id}";
            $this->db->query($sql);

            foreach($product_data['attributes'] as $attribute) {
                $sql = "INSERT INTO product_to_attribute (pta_id, pta_attribute_id) VALUES({$product_id},{$attribute})";
                $this->db->query($sql);
            }
        }
    }

    public function deleteProduct($product_id) {
        $sql = "DELETE FROM product WHERE pdt_id={$product_id}";
        $this->db->query($sql);

        $sql = "DELETE FROM product_to_category WHERE ptc_id={$product_id}";
        $this->db->query($sql);

        $sql = "DELETE FROM product_to_attribute WHERE pta_id={$product_id}";
        $this->db->query($sql);
    }

    public function saveProduct($user_id, $product_id) {
        $sql = "INSERT INTO product_save (pds_user_id, pds_product_id) VALUES({$user_id},{$product_id})";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function saveProductDelete($user_id, $product_id) {
        $sql = "DELETE FROM product_save WHERE pds_user_id={$user_id} AND pds_product_id={$product_id}";
        return $this->db->query($sql);

    }

    public function getHomeProducts($user_id) {
        $sql = "SELECT * FROM product LEFT JOIN product_save ON pdt_id=pds_product_id LEFT JOIN product_to_category ON pdt_id=ptc_id WHERE pds_user_id={$user_id}";
        $result = $this->db->query($sql)->result_array();
        
        $cart = array();
        foreach ($result as $k=> $v) {
            $r1 = array(
                'product_id' => $v['pdt_id'],
                'product_name' => $v['pdt_name'],
                'product_description' => $v['pdt_description'],
                'product_price' => $v['pdt_price'],
                'image_url' => $v['pdt_image_id'],
                'category_id' => $v['ptc_category_id']
            );
            
            $cart[$k] = $r1;
        }
        
        $sql = "SELECT * FROM product LEFT JOIN product_to_category ON pdt_id=ptc_id GROUP BY ptc_category_id ORDER BY pdt_id";
        $result = $this->db->query($sql)->result_array();
        
        $home = array();
        foreach ($result as $k=> $v) {
            $r1 = array(
                'product_id' => $v['pdt_id'],
                'product_name' => $v['pdt_name'],
                'product_description' => $v['pdt_description'],
                'product_price' => $v['pdt_price'],
                'image_url' => $v['pdt_image_id'],
                'category_id' => $v['ptc_category_id']
            );
            $home[$k] = $r1;
        }
        
        $result = array('cart' => $cart, 'home' => $home);
        return $result;
    }
    
    // public function getCategory($category_id) {
    //     $sql = "SELECT * FROM category WHERE ctg_id={$category_id}";
    //     $query = $this->db->query($sql);
    //     return $query->result_array();

    // }

    // public function getAttributeGroup($atg_id) {   
    //     $att_result = array();

    //     $sql = "SELECT * FROM attribute_group WHERE atg_id={$atg_id}";
    //     $query = $this->db->query($sql);
    //     $result_array = $query->result_array();
    //     $att_result['attribute_group'] = $result_array[0];
        
    //     $sql = "SELECT * FROM attribute WHERE atb_atg_id={$atg_id}";
    //     $query = $this->db->query($sql);
    //     $att_result['attribute'] = $query->result_array();
    // }

}

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Product"))
 */
 
class Products_model {
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pdt_id;
    /**
     * @SWG\Property()
     * @var string
     */
    public $pdt_name;
    /**
     * @var string
     * @SWG\Property()
     */
    public $pdt_description;
    /**
     * @var float
     * @SWG\Property()
     */
    public $pdt_price;
    /**
     * @var int
     * @SWG\Property()
     */
    public $ptc_category_id;
    /**
     * @var int
     * @SWG\Property()
     */
    public $pta_attribute_id;
    /**
     * @var string
     * @SWG\Property()
     */
    public $access_token;
}