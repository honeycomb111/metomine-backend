<form>
    <input type="hidden" name="operate_type" value="<?php echo (isset($category['ctg_id']) ? $category['ctg_id'] : 0)?>">
    <fieldset class="margin-30">
        <div class="form-group">
            <label for="CategoryInputName">Name</label>
            <input type="text" class="form-control" id="CategoryInputName" name="ctg_name" placeholder="Name" value="<?php echo (isset($category['ctg_name'])?$category['ctg_name']:'');?>">
        </div>
        <div class="form-group">
            <label for="CategoryStatusSelect">Status</label>
            <select class="form-control" id="CategoryStatusSelect" name="ctg_disabled">
                <?php if(isset($category['ctg_disabled'])):?>
                    <option value="0" <?php echo(($category['ctg_disabled'] == 0) && isset($category['ctg_disabled']) ? 'selected' : '') ?>>Enable</option>
                    <option value="1" <?php echo(($category['ctg_disabled'] == 1) && isset($category['ctg_disabled']) ? 'selected' : '') ?>>Disable</option>
                <?php else:?>
                    <option value="0">Enable</option>
                    <option value="1">Disable</option>
                <?php endif;?>
            </select>
        </div>
        <a class="btn btn-primary" href="<?php echo site_url('/category/');?>">Back</a>
        <button type="submit" class="btn btn-success fright">Save</button>
    </fieldset>
</form>
<?php
    if(isset($_GET['operate_type']) && isset($_GET['ctg_name']) && isset($_GET['ctg_disabled'])){
        $Category_model = new Category_model();
        $operate_type = $_GET['operate_type'];
        $category_data = array('ctg_name'=>html_escape($_GET['ctg_name']),'ctg_id'=>$_GET['ctg_id'], 'ctg_disabled'=>$_GET['ctg_disabled']);
        if($operate_type == 0){
            $Category_model->addCategory($category_data);
        }else{
            $Category_model->updateCategory($operate_type, $category_data);
        }
        redirect('/category');
    }
?>