<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct() {
	 	parent::__construct();
	 	
	 }
	public function index()
	{
		$array = array(array(2,3), array(1,4));
		var_dump(serialize($array));

		$this->load->model('Product_model');
		
		$result = $this->Product_model->getHomeProducts(4);
		
		var_dump($result); exit;
		
		$this->load->helper('url');

		$this->load->model('Discovery_model');
		
		$_SESSION['Astart_time'] = $this->milleseconds();
		
		// var_dump($this->session->userdata('dim2_user_sess'));

		// $this->Discovery_model->setPopSession();
		// var_dump($this->session->userdata('score_pop_sess'));

		// $this->Discovery_model->swipe(42,'up');
		// $this->Discovery_model->swipe(42,'up');
		// $this->Discovery_model->swipe(42,'up');
		// $this->Discovery_model->swipe(42,'up');
		// $this->Discovery_model->swipe(42,'up');
		$this->Discovery_model->discovery_start(7);


		$start_time = $_SESSION['Astart_time'];
        $end_time = $this->milleseconds();

		$_SESSION['Astart_time'] = $end_time;

        $duration = $end_time - $start_time;
		var_dump("Discovery start - " . $duration);

		$final_score = $this->Discovery_model->swipe(42,'up');
		
		$start_time = $_SESSION['Astart_time'];
        $end_time = $this->milleseconds();

		$_SESSION['Astart_time'] = $end_time;

        $duration = $end_time - $start_time;
		var_dump("Swipe - " . $duration);

		
		var_dump($final_score); 
		
		$this->Discovery_model->discovery_end();

		$start_time = $_SESSION['Astart_time'];
        $end_time = $this->milleseconds();

		$_SESSION['Astart_time'] = $end_time;

        $duration = $end_time - $start_time;
		var_dump("Discovery End - " . $duration);
		
		// $this->load->view('welcome_message');


	}

	private function milleseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
	}
	
}
