<?php
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Category"))
 */

class Category_model extends CI_Model {

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $ctg_id;
    
    /**
     * @SWG\Property()
     * @var string
     */
    public $ctg_name;
    
    public function getAllCategory($category_id = 0) {
        if($category_id > 0){
            $sql = "SELECT * FROM category WHERE ctg_id = {$category_id} ";
        }else{
            $sql = "SELECT * FROM category WHERE ctg_id > 0";
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return ($category_id > 0)?$result[0]:$result;
    }

    public function getEnablecategories() {
        $sql = "SELECT * FROM category WHERE ctg_disabled = 0";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function getCategory($category_id) {
        $sql = "SELECT * FROM category WHERE ctg_id={$category_id}";
        $result = $this->db->query($sql)->result_array();
        return $result[0];
    }

    public function getCategories() {
        $sql = "SELECT * FROM category";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function addCategory($category_data) {
        $sql = "INSERT INTO category (ctg_name, ctg_disabled) VALUES ('{$category_data['ctg_name']}', {$category_data['ctg_disabled']})";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function updateCategory($category_id, $category_data) {
        $sql = "UPDATE category SET ctg_name='{$category_data['ctg_name']}', ctg_disabled={$category_data['ctg_disabled']} WHERE ctg_id={$category_id}";
        return $this->db->query($sql);
    }

    public function deleteCategory($category_id) {
        $sql = "DELETE FROM category WHERE ctg_id = {$category_id}";
        return $this->db->query($sql);
    }
}