<?php
class Products extends CI_Controller {

	public function index(){
		$data['title'] = 'Product';
		$data['products'] = $this->Product_model->getAllProduct();
		$this->load->view('templates/header');
		$this->load->view('products/index', $data);
		$this->load->view('templates/footer');
		// $this->load->helper('form_helper');
	}

	public function operate($id){
		$data = array();
		$category_arr = $this->Category_model->getEnablecategories();
		$attribute_arr = $this->Attribute_model->getAllAttributes();
		$attribute_group_arr = $this->Attribute_model->getAttributeGroups();
		$product_to_attribute = $this->Product_model->getProductAttribute();
		$attribute_result = $attribute_group_arr;
		foreach ($attribute_group_arr as $key => $value) {
			foreach ($attribute_arr as $key1 => $value1 ) {
				if($value['atg_id'] == $value1['atb_atg_id']){
					$attribute_result[$key]['attribute'][] = $value1;
				}
			}
		}
		if($id != 'add'){
			$filter_array['pdt_id'] = $id;
			$data['product'] = $this->Product_model->getAllProduct($id);
			if(empty($data['product'])){
				show_404();
			}
			$data['pdt_id'] = $data['product']['pdt_id'];
			$data['pdt_name'] = $data['product']['pdt_name'];
			$data['pdt_description'] = $data['product']['pdt_description'];
			$data['pdt_price'] = $data['product']['pdt_price'];
		}
		$data['category'] = $category_arr;
//		$data['attribute'] = $attribute_arr;
		$data['attribute_data'] = $attribute_result;
		$data['product_to_attribute'] = $product_to_attribute;
//		$data['attributegroup'] = $attribute_group_arr;
		
		// var_dump($data); exit; 
		$this->load->view('templates/header');
		$this->load->view('products/operate', $data);
		$this->load->view('templates/footer');
	}
	public function remove($id){
		$this->Product_model->deleteProduct($id);
		redirect('/products');
	}
}
