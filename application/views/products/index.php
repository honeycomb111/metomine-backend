<h2><?php echo $title?></h2>
<p><a class="fright btn btn-success mbottom-20" href="<?php echo site_url('/products/add');?>">Add</a></p>
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th class="width-5 text-center" scope="col">No</th>
        <th class="width-20 text-center" scope="col">Name</th>
        <th class="width-20 text-center" scope="col">Category</th>
        <th class="width-40 text-center" scope="col">Description</th>
        <th class="width-15 text-center" scope="col">Operate</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($products) > 0):?>
    <?php foreach($products as $product):?>
    <tr>
        <td class="text-center"><?php echo $product['pdt_id']?></td>
        <td><?php echo $product['pdt_name']?></td>
        <td><?php echo $product['ctg_name']?></td>
        <td><?php echo $product['pdt_description']?></td>
        <td class="text-center">
            <a class="btn btn-success" href="<?php echo site_url('/products/'.$product['pdt_id']);?>">Edit</a>
            <a class="btn btn-success" href="<?php echo site_url('/products/remove/'.$product['pdt_id']);?>">Delete</a>
        </td>
    </tr>
    <?php endforeach;?>
    <?php else: ?>
    <tr>
        <td class="text-center" colspan="5">There is no data</td>
    </tr>
    <?php endif;?>
    </tbody>
</table>
