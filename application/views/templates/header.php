<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Shopping Manager</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/style.css">
</head>
<body>
<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3><a class="nav-link" href="<?php echo base_url();?>products">Manage</a></h3>
        </div>

        <ul class="list-unstyled components">
            <li class="mbottom-20">
                <a class="nav-link" href="<?php echo base_url();?>products"><i class="glyphicon glyphicon-king"></i>Product</a>
            </li>
            <li class="mbottom-20">
                <a class="nav-link" href="<?php echo base_url();?>category"><i class="glyphicon glyphicon-th"></i>Category</a>
            </li>
            <li class="mbottom-20">
                <a class="nav-link"  href="<?php echo base_url();?>attributegroup"><i class="glyphicon glyphicon-leaf"></i>AttributeGroup</a>
            </li>
            <li class="mbottom-20">
                <a class="nav-link" href="<?php echo base_url();?>users"><i class="glyphicon glyphicon-user"></i>User</a>
            </li>
        </ul>
    </nav>
    <!-- Page Content Holder -->
    <div id="content" class="width-85">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>products">Product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>category">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  href="<?php echo base_url();?>attributegroup">AttributeGroup</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>users">User</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                });
                $('#sidebar ul li').click(function() {
                    this.toggleClass('active');
                });
            });
        </script>
</body>
</html>
