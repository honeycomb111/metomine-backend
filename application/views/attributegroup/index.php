<h2><?php echo $title?></h2>
<p><a class="fright mbottom-20 btn btn-success" href="<?php echo site_url('/attributegroup/add');?>">Add</a></p>
<table class="table table-hover table-bordered">
    <thead>
    <tr class="table-success">
        <th class="width-5 text-center" scope="col">No</th>
        <th class="width-55 text-center" scope="col">Name</th>
        <th class="width-15 text-center" scope="col" >Operate</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($attribute_group) > 0):?>
    <?php foreach($attribute_group as $attribute_group_val):?>
    <tr>
        <td class="text-center"><?php echo $attribute_group_val['atg_id']?></td>
        <td><?php echo $attribute_group_val['atg_name']?></td>
        <td class="text-center">
            <a class="btn btn-success" href="<?php echo site_url('/attributegroup/'.$attribute_group_val['atg_id']);?>">Edit</a>
            <a class="btn btn-success" href="<?php echo site_url('/attributegroup/remove/'.$attribute_group_val['atg_id']);?>">Delete</a>
        </td>
    </tr>
    <?php endforeach;?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="3">There is no data</td>
        </tr>
    <?php endif;?>
    </tbody>
</table>
