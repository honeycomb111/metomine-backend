<?php

class Discovery_model extends CI_Model {
    const MAX_INF = 10000000000000;
    const MIN_INF = -10000000000000;
    private $atgs;
    private $atbs;
    private $rels;
    private $noveltyFactor;
    private $swipeValue;
    private $categoryId;
    private $products;
    private $productAttributes;
    

    public function __construct() {
        $this->load->library('session');

        $this->getAttributeGroups();
        $this->getAttributes();
        $this->getNoveltyFactor();
    }

    private function getNoveltyFactor() {
        $sql = "SELECT * FROM noveltyfactor";
        $result = $this->db->query($sql)->result_array()[0];
        $this->noveltyFactor = $result['factor'];
    }

    private function getSwipeValue($category_id) {
        $sql = "SELECT * FROM category_swipe_value WHERE csv_id={$category_id}";
        $result_array = $this->db->query($sql)->result_array();

        if (isset($result_array[0])) {
            $result = $result_array[0];
            $swipeValue = array(
                'up' => $result['csv_up'],
                'down' => $result['csv_down'],
                'left' => $result['csv_left'],
                'right' => $result['csv_right']
            );
    
            $this->swipeValue = $swipeValue;    
        }
        
    }

    private function getAttributeGroups() {
        $sql = "SELECT * FROM attribute_group ORDER BY atg_id";
        $query= $this->db->query($sql);
        $result = $query->result_array();

        $atg = array();
        foreach($result as $k=>$v) {
            $atg[$k] = $v['atg_id'];
        }
        $this->atgs = $atg;
        return $atg;
    }

    private function getAttributes() {

        $sql = "SELECT * FROM attribute ORDER BY atb_atg_id, atb_id";
        $query= $this->db->query($sql);
        $result = $query->result_array();

        $atb = array();
        $rel = array();
        foreach($result as $k=>$v) {
            $atb[$k] = $v['atb_id'];
            $rel[$v['atb_id']] = $v['atb_atg_id'];
        }
        $this->atbs = $atb;
        $this->rels = $rel;
        return $rel;
    }

    private function getDim1PopSess() {
        $sql = "SELECT * FROM dim1_population";
        $result = $this->db->query($sql)->result_array();

        $dim1_pop_sess = array();
        foreach($result as $value) {
            $dim1_pop_sess[$value['atb']] = $value['d1p_value'];
        }
        return $dim1_pop_sess;
    }

    private function getDim2PopSess() {
        $sql = "SELECT * FROM dim2_population";
        $result = $this->db->query($sql)->result_array();

        $dim2_pop_sess = array();
        foreach($result as $value) {
            $dim2_pop_sess[$value['atb1']][$value['atb2']] = $value['d2p_value'];
        }

        return $dim2_pop_sess;
    }

    private function getDim3PopSess() {
        $sql = "SELECT * FROM dim3_population";
        $result = $this->db->query($sql)->result_array();

        $dim3_pop_sess = array();
        foreach($result as $value) {
            $dim3_pop_sess[$value['atb1']][$value['atb2']][$value['atb3']] = $value['d3p_value'];
        }
        return $dim3_pop_sess;
    }

    private function setPopSession() {
        // TABLE_DIMENSION1_POP_SESSION
        $dim1_pop_sess = $this->getDim1PopSess();

        $dim1_max_pop_sess = array();
        $dim1_min_pop_sess = array();
        $dim1_range_pop_sess = array();
        $dim1_weight_pop_sess = array();
        $dim1_normalized_pop_sess = array();

        $dim1_max = self::MIN_INF;
        foreach($this->atbs as $atb) {
            $atg = $this->rels[$atb];

            $compare = isset($dim1_pop_sess[$atb]) ? $dim1_pop_sess[$atb] : 0;
            if (!isset($dim1_max_pop_sess[$atg])) $dim1_max_pop_sess[$atg] = self::MIN_INF;
            $dim1_max_pop_sess[$atg] = max($dim1_max_pop_sess[$atg], $compare);
            $dim1_max = max($dim1_max, $dim1_max_pop_sess[$atg]);

            if (!isset($dim1_min_pop_sess[$atg])) $dim1_min_pop_sess[$atg] = self::MAX_INF;
            $dim1_min_pop_sess[$atg] = min($dim1_min_pop_sess[$atg], $compare);
        }

        if ($dim1_max == self::MIN_INF) $dim1_max = 0; 

        foreach($this->atgs as $atg) { 
            if (!isset($dim1_max_pop_sess[$atg]) || $dim1_max_pop_sess[$atg] == self::MIN_INF) $dim1_max_pop_sess[$atg] = 0;
            if (!isset($dim1_min_pop_sess[$atg]) || $dim1_min_pop_sess[$atg] == self::MAX_INF) $dim1_min_pop_sess[$atg] = 0;
            $dim1_range_pop_sess[$atg] = $dim1_max_pop_sess[$atg] - $dim1_min_pop_sess[$atg];

            if ($dim1_max == 0) {
                $dim1_weight_pop_sess[$atg] = 0;
            } else {
                $dim1_weight_pop_sess[$atg] = $dim1_range_pop_sess[$atg] / $dim1_max;
            }
            
        }
        
        foreach($this->atbs as $atb) {
            if ($dim1_max_pop_sess[$atg] == 0){
                $dim1_normalized_pop_sess[$atb] = 0;
            } else {
                if (isset($dim1_pop_sess[$atb])) {
                    $dim1_normalized_pop_sess[$atb] = $dim1_pop_sess[$atb] * $dim1_weight_pop_sess[$atg] / $dim1_max_pop_sess[$atg]; 
                } else {
                    $dim1_normalized_pop_sess[$atb] = 0;
                }
            }
        }

        $this->session->set_userdata('dim1_pop_sess', $dim1_pop_sess);

        // TABLE_DIMENSION2_POP_SESSION
        $dim2_pop_sess = $this->getDim2PopSess();
        $dim2_max = self::MIN_INF;
        foreach($dim2_pop_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                if ($atb1 <> $atb2) $dim2_max = max($dim2_max, $dim2_pop_sess[$atb1][$atb2]);
            }
        }
        if ($dim2_max == self::MIN_INF) $dim2_max = 0;
        
        $dim2_normalized_pop_sess = array();
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                if ($atb1 <> $atb2) {
                    $atg1 = $this->rels[$atb1];
                    $atg2 = $this->rels[$atb2];
                    if ($dim2_max == 0) {
                        $dim2_normalized_pop_sess[$atb1][$atb2] = 0;    
                    } else {
                        if (isset($dim2_pop_sess[$atb1][$atb2])) {
                            $dim2_normalized_pop_sess[$atb1][$atb2] = $dim2_pop_sess[$atb1][$atb2] / $dim2_max * $dim1_weight_pop_sess[$atg1] * $dim1_weight_pop_sess[$atg2];
                        } else {
                            $dim2_normalized_pop_sess[$atb1][$atb2] = 0;
                        }
                    }
                }
            }
        }

        $this->session->set_userdata('dim2_pop_sess', $dim2_pop_sess);

        // TABLE_DIMENSION3_POP_SESSION
        $dim3_pop_sess = $this->getDim3PopSess();

        $dim3_max = self::MIN_INF;
        foreach($dim3_pop_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                foreach($val2 as $atb3=>$val3) {
                    if ($atb1 <> $atb2 && $atb1 <> $atb3 && $atb2 <> $atb3){
                        $dim3_max = max($dim3_max, $dim3_pop_sess[$atb1][$atb2][$atb3]);
                    } 
                }
            }
        }
        if ($dim3_max == self::MIN_INF) $dim3_max = 0;

        $dim3_normalized_pop_sess = array();
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                foreach($this->atbs as $atb3) {
                    if ($atb1 <> $atb2 && $atb1 <> $atb3 && $atb2 <> $atb3) {
                        $atg1 = $this->rels[$atb1];
                        $atg2 = $this->rels[$atb2];
                        $atg3 = $this->rels[$atb3];
                        if ($dim3_max == 0) {
                            $dim3_normalized_pop_sess[$atb1][$atb2][$atb3] = 0;
                        } else {
                            if (isset($dim3_pop_sess[$atb1][$atb2][$atb3])) {
                                $dim3_normalized_pop_sess[$atb1][$atb2][$atb3] = $dim3_pop_sess[$atb1][$atb2][$atb3] / $dim3_max * $dim1_weight_pop_sess[$atg1] * $dim1_weight_pop_sess[$atg2] * $dim1_weight_pop_sess[$atg3];
                            } else {
                                $dim3_normalized_pop_sess[$atb1][$atb2][$atb3] = 0;
                            }
                        }
                    }
                }
            }
        }

        $this->session->set_userdata('dim3_pop_sess', $dim3_pop_sess);

        $score_pop_sess = array();
        foreach($this->products as $product) {
            $dim1_score = 0;
            $dim1_tot = 0;
            foreach($this->productAttributes[$product] as $attribute) {
                $atg = $this->rels[$attribute];
                $dim1_score += $dim1_normalized_pop_sess[$attribute];
                $dim1_tot++;
            }
            $dim1_pop_score = $dim1_score / $dim1_tot;

            $dim2_score = 0;
            $dim2_tot = 0;
            foreach($this->productAttributes[$product] as $atb1) {
                foreach($this->productAttributes[$product] as $atb2) {
                    if ($atb1 < $atb2) {
                        $dim2_score += $dim2_normalized_pop_sess[$atb1][$atb2];
                        $dim2_tot++;
                    }
                }
            }
            $dim2_pop_score = $dim2_score / $dim2_tot;

            $dim3_score = 0;
            $dim3_tot = 0;
            foreach($this->productAttributes[$product] as $atb1) {
                foreach($this->productAttributes[$product] as $atb2) {
                    foreach($this->productAttributes[$product] as $atb3) {
                        if ($atb1 < $atb2 && $atb2 < $atb3) {
                            $dim3_score += $dim3_normalized_pop_sess[$atb1][$atb2][$atb3];
                            $dim3_tot++;
                        }
                    }
                }
            }
            $dim3_pop_score = $dim3_score / $dim3_tot;
            $pop_score = $dim1_pop_score + $dim2_pop_score + $dim3_pop_score;

            $score_pop_sess[$product] = $pop_score;
        }
        $this->session->set_userdata('score_pop_sess', $score_pop_sess);

        arsort($score_pop_sess);
        $product_id = array_keys($score_pop_sess)[0];
        return $product_id;
    }

    private function getCategoryId($product_id) {
        $sql = "SELECT ptc_category_id FROM product_to_category WHERE ptc_id={$product_id}";
        $result = $this->db->query($sql)->result_array()[0];
        return $result['ptc_category_id'];
    }

    private function getAttributesFromProduct($product_id) {
        $sql = "SELECT * FROM product_to_attribute WHERE pta_id={$product_id}";
        $result = $this->db->query($sql)->result_array();

        $attributes = array();
        foreach($result as $k=>$v) {
            $attributes[$k] = $v['pta_attribute_id'];
        }
        return $attributes;
    }

    private function getProductFromCategory($category_id) {
        $sql = "SELECT * FROM product_to_category WHERE ptc_category_id={$category_id}";
        $result = $this->db->query($sql)->result_array();

        $products = array();
        foreach($result as $k=>$v) {
            $products[$k] = $v['ptc_id'];
        }
        return $products;
    }

    public function getProductAttributes($products) {
        $products_string = implode(",", $products);
        $sql = "SELECT * FROM product_to_attribute WHERE pta_id IN ({$products_string})";
        $result = $this->db->query($sql)->result_array();

        $product_attributes = array();
        foreach ($result as $value) {
            $product_attributes[$value["pta_id"]][] = $value["pta_attribute_id"];
        }
        return $product_attributes;
    }

    private function getProducts() {
        $sql = "SELECT * FROM product ORDER BY pdt_id";
        $result = $this->db->query($sql)->result_array();

        $products = array();
        foreach($result as $k=>$v) {
            $products[$k] = $v['pdt_id'];
        }
        return $products;
    }

    private function insertDim1Population($atb, $value) {
        $sql = "INSERT INTO dim1_population (atb, d1p_value) VALUES ({$atb}, {$value})";
        $this->db->query($sql);
    }

    private function updateDim1Population($atb, $value) {
        $sql = "UPDATE dim1_population SET d1p_value=d1p_value+{$value} WHERE atb={$atb}";
        $this->db->query($sql);
    }

    private function insertDim2Population($atb1, $atb2, $value) {
        $sql = "INSERT INTO dim2_population (atb1, atb2, d2p_value) VALUES ({$atb1},{$atb2},{$value})";
        $this->db->query($sql);
    }

    private function updateDim2Population($atb1, $atb2, $value) {
        $sql = "UPDATE dim2_population SET d2p_value=d2p_value+{$value} WHERE atb1={$atb1} AND atb2={$atb2}";    
        $this->db->query($sql);
    }

    private function insertDim3Population($atb1, $atb2, $atb3, $value) {
        $sql = "INSERT INTO dim3_population (atb1, atb2, atb3, d3p_value) VALUES ({$atb1},{$atb2},{$atb3},{$value})";
        $this->db->query($sql);
    }

    private function updateDim3Population($atb1, $atb2, $atb3, $value) {
        $sql = "UPDATE dim3_population SET d3p_value=d3p_value+{$value} WHERE atb1={$atb1} AND atb2={$atb2} AND atb3={$atb3}";
        $this->db->query($sql);
    }

    public function discovery_start($category_id) {
        $_SESSION['start_time'] = $this->milleseconds();
        $this->getSwipeValue($category_id);
        $this->categoryId = $category_id;

        $start_time = $_SESSION['start_time'];
        $end_time = $this->milleseconds();
		$_SESSION['start_time'] = $end_time;
        $duration = $end_time - $start_time;
        var_dump("SwipeValue - " .$duration);
        
        $this->products = $this->getProductFromCategory($category_id);
        $this->productAttributes = $this->getProductAttributes($this->products);
        
        $start_time = $_SESSION['start_time'];
        $end_time = $this->milleseconds();
		$_SESSION['start_time'] = $end_time;
        $duration = $end_time - $start_time;
        var_dump("GetProduct_Attributes - " .$duration);

        $product_id = $this->setPopSession();
        
        return $product_id;
        $start_time = $_SESSION['start_time'];
        $end_time = $this->milleseconds();
		$_SESSION['start_time'] = $end_time;
        $duration = $end_time - $start_time;
        var_dump("setPopSession - " .$duration);
    }

    public function discovery_end() {
        $dim1_pop_sess = $this->session->userdata('dim1_pop_sess');
        $dim1_user_sess = $this->session->userdata('dim1_user_sess');

        foreach($dim1_user_sess as $atb=>$val) {
            if ($val <> 0) {
                if (isset($dim1_pop_sess[$atb])) {
                    $this->updateDim1Population($atb, $val);
                } else {
                    $this->insertDim1Population($atb, $val);
                }
            }
        }

        $dim2_pop_sess = $this->session->userdata('dim2_pop_sess');
        $dim2_user_sess = $this->session->userdata('dim2_user_sess');

        foreach($dim2_user_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                if ($val2 <> 0) {
                    if (isset($dim2_pop_sess[$atb1][$atb2])) {
                        $this->updateDim2Population($atb1, $atb2, $val2);
                    } else {
                        $this->insertDim2Population($atb1, $atb2, $val2);
                    }
                }
            }            
        }

        $dim3_pop_sess = $this->session->userdata('dim3_pop_sess');
        $dim3_user_sess = $this->session->userdata('dim3_user_sess');

        foreach($dim3_user_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                foreach($val2 as $atb3=>$val3) {
                    if ($val3 <> 0) {
                        if (isset($dim3_pop_sess[$atb1][$atb2][$atb3])) {
                            $this->updateDim3Population($atb1, $atb2, $atb3, $val3);
                        } else {
                            $this->insertDim3Population($atb1, $atb2, $atb3, $val3);
                        }
                    }    
                }
            }            
        }

        $this->session->unset_userdata('dim1_user_sess');
        $this->session->unset_userdata('dim2_user_sess');
        $this->session->unset_userdata('dim3_user_sess');
        $this->session->unset_userdata('dim1_pop_sess');
        $this->session->unset_userdata('dim2_pop_sess');
        $this->session->unset_userdata('dim3_pop_sess');
        $this->session->unset_userdata('score_pop_sess');
    }

    public function swipe($product_id, $direction) {

        $value = $this->swipeValue[$direction];
        $attributes = $this->productAttributes[$product_id];
        
        // TABLE_DIMENSION1_USER_SESSION
        $dim1_user_sess = $this->session->userdata('dim1_user_sess');
        if ($dim1_user_sess === NULL) $dim1_user_sess = array();
        foreach($attributes as $atb) {
            if (isset($dim1_user_sess[$atb])){
                $dim1_user_sess[$atb] += $value;
            } else {
                $dim1_user_sess[$atb] = $value;
            }
        }
        $this->session->set_userdata('dim1_user_sess', $dim1_user_sess); 

        $dim1_max_user_sess = array();
        $dim1_min_user_sess = array();
        $dim1_range_user_sess = array();
        $dim1_weight_user_sess = array();
        $dim1_normalized_user_sess = array();

        $dim1_max = self::MIN_INF;
        foreach($this->atbs as $atb) {
            $atg = $this->rels[$atb];

            $compare = isset($dim1_user_sess[$atb]) ? $dim1_user_sess[$atb] : 0;
            
            if (!isset($dim1_max_user_sess[$atg])) $dim1_max_user_sess[$atg] = self::MIN_INF;
            $dim1_max_user_sess[$atg] = max($dim1_max_user_sess[$atg], $compare);
            $dim1_max = max($dim1_max, $dim1_max_user_sess[$atg]);

            if (!isset($dim1_min_user_sess[$atg])) $dim1_min_user_sess[$atg] = self::MAX_INF;
            $dim1_min_user_sess[$atg] = min($dim1_min_user_sess[$atg], $compare);
            
        } 

        if ($dim1_max == self::MIN_INF) $dim1_max = 0; 

        foreach($this->atgs as $atg) {
            if (!isset($dim1_max_user_sess[$atg]) || $dim1_max_user_sess[$atg] == self::MIN_INF) $dim1_max_user_sess[$atg] = 0;
            if (!isset($dim1_min_user_sess[$atg]) || $dim1_min_user_sess[$atg] == self::MAX_INF) $dim1_min_user_sess[$atg] = 0;

            $dim1_range_user_sess[$atg] = $dim1_max_user_sess[$atg] - $dim1_min_user_sess[$atg];
            if ($dim1_max == 0) {
                $dim1_weight_user_sess[$atg] = 0;    
            } else {
                $dim1_weight_user_sess[$atg] = $dim1_range_user_sess[$atg] / $dim1_max;
            }
        }

        foreach($this->atbs as $atb) {
            $atg = $this->rels[$atb];
            if ($dim1_max_user_sess[$atg] == 0) {
                $dim1_normalized_user_sess[$atb] = 0;
            } else {
                if (isset($dim1_user_sess[$atb])) {
                    $dim1_normalized_user_sess[$atb] = $dim1_user_sess[$atb] * $dim1_weight_user_sess[$atg] / $dim1_max_user_sess[$atg]; 
                } else {
                    $dim1_normalized_user_sess[$atb] = 0;
                }
                
            }
        }

        // TABLE_DIMENSION2_USER_SESSION
        $dim2_user_sess = $this->session->userdata('dim2_user_sess');
        if ($dim2_user_sess === NULL) $dim2_user_sess = array();
        foreach($attributes as $atb1) {
            foreach($attributes as $atb2) {
                if ($atb1 <> $atb2) {
                    if (isset($dim2_user_sess[$atb1][$atb2])) {
                        $dim2_user_sess[$atb1][$atb2] += $value;
                    } else {
                        $dim2_user_sess[$atb1][$atb2] = $value;
                    }
                }
            }
        }
        
        $this->session->set_userdata('dim2_user_sess', $dim2_user_sess);

        $dim2_max = self::MIN_INF;
        foreach($dim2_user_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                $dim2_max = max($dim2_max, $dim2_user_sess[$atb1][$atb2]);
            }
        }

        if ($dim2_max == self::MIN_INF) $dim2_max = 0;
        $dim2_normalized_user_sess = array();
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                $atg1 = $this->rels[$atb1];
                $atg2 = $this->rels[$atb2];
                if ($dim2_max == 0) {
                    $dim2_normalized_user_sess[$atb1][$atb2] = 0;    
                } else {
                    if (isset($dim2_user_sess[$atb1][$atb2])) {
                        $dim2_normalized_user_sess[$atb1][$atb2] = $dim2_user_sess[$atb1][$atb2] / $dim2_max * $dim1_weight_user_sess[$atg1] * $dim1_weight_user_sess[$atg2];
                    } else {
                        $dim2_normalized_user_sess[$atb1][$atb2] = 0;    
                    }
                }
            }
        }

        // TABLE_DIMENSION3_USER_SESSION
        $dim3_user_sess = $this->session->userdata('dim3_user_sess');
        if ($dim3_user_sess === NULL) $dim3_user_sess = array();
        foreach($attributes as $atb1) {
            foreach($attributes as $atb2) {
                foreach($attributes as $atb3) {
                    if ($atb1 <> $atb2 && $atb1 <> $atb3 && $atb2 <> $atb3) {
                        if (isset($dim3_user_sess[$atb1][$atb2][$atb3])) {
                            $dim3_user_sess[$atb1][$atb2][$atb3] += $value;
                        } else {
                            $dim3_user_sess[$atb1][$atb2][$atb3] = $value;
                        }
                    } 
                }
            }
        }
        $this->session->set_userdata('dim3_user_sess', $dim3_user_sess);

        $dim3_max = self::MIN_INF;
        foreach($dim3_user_sess as $atb1=>$val1) {
            foreach($val1 as $atb2=>$val2) {
                foreach($val2 as $atb3=>$val3) {
                    if ($atb1 <> $atb2 && $atb1 <> $atb3 && $atb2 <> $atb3) {
                        $dim3_max = max($dim3_max, $dim3_user_sess[$atb1][$atb2][$atb3]);
                    }
                }
            }
        }
        if ($dim3_max == self::MIN_INF) $dim3_max = 0;

        $dim3_normalized_user_sess = array();
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                foreach($this->atbs as $atb3) {
                    $atg1 = $this->rels[$atb1];
                    $atg2 = $this->rels[$atb2];
                    $atg3 = $this->rels[$atb3];
                    if ($dim3_max == 0){
                        $dim3_normalized_user_sess[$atb1][$atb2][$atb3] = 0;    
                    } else {
                        if (isset($dim3_user_sess[$atb1][$atb2][$atb3])) {
                            $dim3_normalized_user_sess[$atb1][$atb2][$atb3] = $dim3_user_sess[$atb1][$atb2][$atb3] / $dim3_max * $dim1_weight_user_sess[$atg1] * $dim1_weight_user_sess[$atg2] * $dim1_weight_user_sess[$atg3];
                        } else {
                            $dim3_normalized_user_sess[$atb1][$atb2][$atb3] = 0;
                        }
                    }
                }
            }
        }

        // Get user score
        $score_user_sess = array();
        foreach($this->products as $product) {

            $dim1_score = 0;
            $dim1_tot = 0;
            foreach($this->productAttributes[$product] as $atb) {
                $atg = $this->rels[$atb];
                $dim1_score += $dim1_normalized_user_sess[$atb];
                $dim1_tot++;
            }
            $dim1_user_score = $dim1_score / $dim1_tot;

            $dim2_score = 0;
            $dim2_tot = 0;
            foreach($this->productAttributes[$product] as $atb1) {
                foreach($this->productAttributes[$product] as $atb2) {
                    if ($atb1 < $atb2) {
                        $dim2_score += $dim2_normalized_user_sess[$atb1][$atb2];
                        $dim2_tot++;
                    }
                }
            }
            $dim2_user_score = $dim2_score / $dim2_tot;

            $dim3_score = 0;
            $dim3_tot = 0;
            foreach($this->productAttributes[$product] as $atb1) {
                foreach($this->productAttributes[$product] as $atb2) {
                    foreach($this->productAttributes[$product] as $atb3) {
                        if ($atb1 < $atb2 && $atb2 < $atb3) {
                            $dim3_score += $dim3_normalized_user_sess[$atb1][$atb2][$atb3];
                            $dim3_tot++;
                        }
                    }
                }
            }
            $dim3_user_score = $dim3_score / $dim3_tot;
            $user_score = $dim1_user_score + $dim2_user_score + $dim3_user_score;

            $score_user_sess[$product] = $user_score;
        }

        // Get population score
        $score_pop_sess = $this->session->userdata('score_pop_sess');


        // Get TOTALPERCENTAGESEEN_USER_SESSIONVALUE
        $dim1SeenValue = 0 ;
        $dim1SeenTot = 0;
        foreach($this->atbs as $atb) {
            if (isset($dim1_user_sess[$atb])) $dim1SeenValue++;
            $dim1SeenTot++;
        }
        $dim1PerSeenUserSessValue = $dim1SeenValue / $dim1SeenTot;

        $dim2SeenValue = 0;
        $dim2SeenTot = 0;
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                $atg1 = $this->rels[$atb1];
                $atg2 = $this->rels[$atb2];
                if ($atg1 <> $atg2) {
                    if (isset($dim2_user_sess[$atb1][$atb2])) $dim2SeenValue++;
                    $dim2SeenTot++;
                }
            }
        }
        $dim2PerSeenUserSessValue = $dim2SeenValue / $dim2SeenTot;

        $dim3SeenValue = 0;
        $dim3SeenTot = 0;
        foreach($this->atbs as $atb1) {
            foreach($this->atbs as $atb2) {
                foreach($this->atbs as $atb3) {
                    $atg1 = $this->rels[$atb1];
                    $atg2 = $this->rels[$atb2];
                    $atg3 = $this->rels[$atb3];
                    if ($atg1 <> $atg2 && $atg1 <> $atg3 && $atg2 <> $atg3) {
                        if (isset($dim3_user_sess[$atb1][$atb2][$atb3])) $dim3SeenValue++;
                        $dim3SeenTot++;
                    }
                }
            }
        }
        $dim3PerSeenUserSessValue = $dim3SeenValue / $dim3SeenTot;
        $totPerSeenUserSessValue = ($dim1PerSeenUserSessValue + $dim2PerSeenUserSessValue + $dim3PerSeenUserSessValue)/3;

        // Get Factor
        $factor = array();
        foreach($this->products as $product) {

            $factor[$product] = 1;
            foreach($this->productAttributes[$product] as $attribute) {
                if (!isset($dim1_user_sess[$attribute])) $factor[$product] *= $this->noveltyFactor;
            }
        }

        // Get FinalScore
        $final_score = array();
        foreach($this->products as $product) {
            $final_score[$product] = ($score_user_sess[$product] * $totPerSeenUserSessValue + $score_pop_sess[$product] * (1 - $totPerSeenUserSessValue)) * $factor[$product];
        }
        arsort($final_score);
        
        return $final_score;
        $product_id = array_keys($final_score)[0];

        return $product_id;
    }

    private function milleseconds() {
        $mt = explode(' ', microtime());
        return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
    }
}