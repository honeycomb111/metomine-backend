-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2018 at 04:10 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databack`
--

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE `attribute` (
  `atb_id` int(11) NOT NULL,
  `atb_name` varchar(50) NOT NULL,
  `atb_atg_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`atb_id`, `atb_name`, `atb_atg_id`) VALUES
(29, 'Mm', 1),
(30, 'Cm', 1),
(31, 'M', 1),
(32, 'Km', 1),
(33, 'Red', 2),
(34, 'Yellow', 2),
(35, 'White', 2),
(36, 'Blue', 2),
(37, 'Grey', 2),
(38, 'Gram', 3),
(39, 'Kilogram', 3),
(40, 'Tone', 3);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

CREATE TABLE `attribute_group` (
  `atg_id` int(11) NOT NULL,
  `atg_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_group`
--

INSERT INTO `attribute_group` (`atg_id`, `atg_name`) VALUES
(1, 'Size'),
(2, 'Color'),
(3, 'Weight');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ctg_id` int(11) NOT NULL,
  `ctg_name` varchar(50) NOT NULL,
  `ctg_disabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`ctg_id`, `ctg_name`, `ctg_disabled`) VALUES
(7, 'Shoes', 0),
(8, 'Bagsd', 1),
(9, 'Computer', 0),
(10, 'TV', 0),
(12, 'test', 1),
(13, 'sdfsdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `ignore_limits` int(11) NOT NULL,
  `is_private_key` int(11) NOT NULL,
  `ip_addresses` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, 'CODEX@123', 0, 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pdt_id` int(11) NOT NULL,
  `pdt_name` varchar(50) NOT NULL,
  `pdt_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pdt_id`, `pdt_name`, `pdt_description`) VALUES
(41, 'Dell', 'This is a computer.'),
(42, 'Sony', 'This is a TV'),
(43, 'Sunny ', 'Not for sell');

-- --------------------------------------------------------

--
-- Table structure for table `product_save`
--

CREATE TABLE `product_save` (
  `pds_id` int(11) NOT NULL,
  `pds_user_id` int(11) NOT NULL,
  `pds_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_attribute`
--

CREATE TABLE `product_to_attribute` (
  `pta_id` int(11) NOT NULL,
  `pta_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_attribute`
--

INSERT INTO `product_to_attribute` (`pta_id`, `pta_attribute_id`) VALUES
(41, 30),
(41, 34),
(41, 39),
(42, 30),
(42, 35),
(42, 40),
(43, 31),
(43, 37),
(43, 39);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE `product_to_category` (
  `ptc_id` int(11) NOT NULL,
  `ptc_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`ptc_id`, `ptc_category_id`) VALUES
(41, 9),
(42, 10),
(43, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `facebook_uid` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`, `facebook_uid`) VALUES
(4, 'dragon', 'dragon@test.com', 'solomon', NULL),
(5, 'pak2', 'pak@tt.com', 'bbdd0e294fd183663ccadb3d3f94dca1', NULL),
(6, 'kim', 'kim@tr.com', '202cb962ac59075b964b07152d234b70', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`atb_id`);

--
-- Indexes for table `attribute_group`
--
ALTER TABLE `attribute_group`
  ADD PRIMARY KEY (`atg_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ctg_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pdt_id`);

--
-- Indexes for table `product_save`
--
ALTER TABLE `product_save`
  ADD PRIMARY KEY (`pds_id`);

--
-- Indexes for table `product_to_attribute`
--
ALTER TABLE `product_to_attribute`
  ADD PRIMARY KEY (`pta_id`,`pta_attribute_id`);

--
-- Indexes for table `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`ptc_id`,`ptc_category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `atb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `attribute_group`
--
ALTER TABLE `attribute_group`
  MODIFY `atg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `ctg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pdt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `product_save`
--
ALTER TABLE `product_save`
  MODIFY `pds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
