<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Tensai
 * @license         CS
 */
class Product_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Product_model');
    }

    /**
     * @SWG\Get(path="/product/get",
     *   tags={"product"},
     *   summary="Get a product by given id",
     *   description="",
     *   operationId="product_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="product_id",
     *     description="The product id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Product_model")
     *    ),
     * 
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get a product failed"),
     * )
     */
     
    function product_get() {
        if(!$this->get('product_id'))
        {
           $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        
        $product = $this->Product_model->getProduct($this->get('product_id'));
         
        $product = array_merge($product, array('access_token' => $access_token));
        if($product) {
            $this->response($product, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }


    /**
     * @SWG\Get(path="/products/home",
     *   tags={"product"},
     *   summary="Get Homepage products",
     *   description="",
     *   operationId="productsHome_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation"),
     * 
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get products failed"),
     * )
     */
     
    function productsHome_get() {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }
        
        $user_id = $this->session->userdata('user_id');
        if ($user_id) {
            $products = $this->Product_model->getHomeProducts($user_id);
            // $products = array($products, array('access_token' => $access_token));
            
            $result = array('result' => $products, 'access_token'=>$access_token);
            $test = array('debug' => 'test', 'product' => $products);
            
            $this->response($result, 200); // 200 being the HTTP response code
        } else {
            $this->response(NULL, 404);
        }
    }
    
    /**
     * @SWG\Post(path="/product/save",
     *   tags={"product"},
     *   summary="Save a product to cart",
     *   description="",
     *   operationId="saveProduct",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="product_id",
     *     in="query",
     *     description="product id that need to be saved",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Product_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Product save failed"),
     * )
     */
    function productsave_post() {
        if(!$this->post('product_id'))
        {
           $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $user_id = $this->session->userdata('user_id');
        $success = $this->Product_model->saveProduct($user_id, $this->post('product_id'));

        $success = array_merge($success, array('access_token' => $access_token));
        if($success) {
            $this->response($success, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }

/**
     * @SWG\Get(path="/product/down",
     *   tags={"product"},
     *   summary="Delete a product from cart",
     *   description="",
     *   operationId="deleteProduct",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="product_id",
     *     in="query",
     *     description="product id that need to be deleted",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Product_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Product down failed"),
     * )
     */
     
    function productsave_get() {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(array('status_code' =>400 , 'message'=>'token mismatch'), 400);
        }

        if(!$this->get('product_id'))
        {
           $this->response(array('status_code'=>400,'message'=>'missing required params'), 400);
        }

        $user_id = $this->session->userdata('user_id');
        $success = $this->Product_model->saveProductDelete($user_id, $this->get('product_id'));

        $success = array_merge($success, array('access_token' => $access_token,'status_code'=>200));
        if($success) {
            $this->response($success, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status_code'=>404,'message'=>'product save delete failed'), 404);
        }
    }

    /**
     * @SWG\Get(path="/products/get",
     *   tags={"product"},
     *   summary="Get products",
     *   description="",
     *   operationId="getProducts",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="product_id",
     *     in="query",
     *     description="product id that need to be filtered",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="product_name",
     *     in="query",
     *     description="product name that need to be filtered",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="category_id",
     *     in="query",
     *     description="category id that need to be filtered",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="attribute",
     *     in="query",
     *     description="attribute array that need to be filtered",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="attrib",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Products_model")
     *     )
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get products failed"),
     * )
     */
     
    function products_get()
    {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(array('status_code' =>400 , 'message'=>'token mismatch'), 400);
        }

        $product_data = array();
        
        if($this->get('product_id')) {
            $product_data['pdt_id'] = $this->get('product_id');
        }

        if($this->get('product_name')) {
            $product_data['pdt_name'] = $this->get('product_name');
        }

        if($this->get('category_id')) {
            $product_data['category'] = $this->get('category_id');
        }

        if($this->get('attribute')) {
            $product_data['attribute'] = unserialize($this->get('attribute'));
        }

        if(count($product_data) == 0)
        {
            $products = $this->Product_model->getAllProduct();
        }else{
            $products = $this->Product_model->getProducts($product_data);
        }

        $products = array_merge($products, array('access_token' => $access_token,'status_code'=>200));
        if($products) {
            $this->response($products, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status_code'=>404,'message'=>'get products failed'), 404);
        }

    }
}
