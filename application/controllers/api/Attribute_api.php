<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Tensai
 * @license         CS
 */
class Attribute_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Attribute_model');
    }

    /**
     * @SWG\Get(path="/attributes/get",
     *   tags={"attribute"},
     *   summary="Get All Attributes",
     *   description="",
     *   operationId="atgs",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Attribute_model")
     *     )
     *    ),
     *   @SWG\Response(response=401, description="Missing required params"),
     *   @SWG\Response(response=404, description="Get attributes failed"),
     * )
     */
    function atgs_get()
    {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $attributeGroups = $this->Attribute_model->getAttributeGroups();
        $attributeGroups = array_merge($attributeGroups, array('access_token' => $access_token));

        if($attributeGroups) {
            $this->response($attributeGroups, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }

    /**
     * @SWG\Get(path="/attribute/get",
     *   tags={"attribute"},
     *   summary="Get a attribute By given id",
     *   description="",
     *   operationId="atg",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="attribute_id",
     *     description="The attribute id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Attribute_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get a attribute failed"),
     * )
     */
    function atg_get()
    {
        if(!$this->get('attribute_id')) {
            $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $attributeGroup = $this->Attribute_model->getAttributeGroup($this->get('attribute_id'));
        $attributeGroup = array_merge($attributeGroup, array('access_token' => $access_token));

        if($attributeGroup) {
            $this->response($attributeGroup, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status_code'=>404,'message'=>'attribute get failed'), 404);
        }
    }

    /**
     * @SWG\Get(path="/values/get",
     *   tags={"attribute"},
     *   summary="Get Values of a given attribute",
     *   description="",
     *   operationId="atbs",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="attribute_id",
     *     description="The attribute id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Value_model")
     *     )
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get values failed"),
     * )
     */
     
    function atbs_get() 
    {
        if(!$this->get('attribute_id')) {
            $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $attributes = $this->Attribute_model->getAttributes($this->get('attribute_id'));
        $attributes = array_merge($attributes, array('access_token' => $access_token));

        if($attributes)
        {
            $this->response($attributes, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }
    
    /**
     * @SWG\Get(path="/value/get",
     *   tags={"attribute"},
     *   summary="Get Value by given value id",
     *   description="",
     *   operationId="atb",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="value_id",
     *     description="The value id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Value_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get a value failed"),
     * )
     */
     
    function atb_get()
    {
        if(!$this->get('value_id')) {
            $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $attribute = $this->Attribute_model->getAttribute($this->get('value_id'));
        $attribute = array_merge($attribute, array('access_token' => $access_token));

        if($attribute)
        {
            $this->response($attribute, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }
}
