<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Tensai
 * @license         CS
 */
class Discovery_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Discovery_model');
        $this->load->model('Product_model');
    }

    /**
     * @SWG\Get(path="/discovery/start",
     *   tags={"discovery"},
     *   summary="Discovery start",
     *   description="",
     *   operationId="discovery_start",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="product_id",
     *     description="The product id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Product_model")
     *    ),
     * 
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Discovery start failed"),
     * )
     */
     
    function discovery_start_get() {
        if(!$this->get('category_id'))
        {
           $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        
 
        $product_id = $this->Discovery_model->discovery_start($this->get('category_id'));
        
        $product = $this->Product_model->getProduct($product_id);
        $product = array_merge($product, array('access_token' => $access_token));

        if($product) {
            $this->response($product, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }

/**
     * @SWG\Get(path="/discovery/swipe",
     *   tags={"discovery"},
     *   summary="Discovery swipe",
     *   description="",
     *   operationId="discovery_swipe",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="product_id",
     *     description="The product id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="direction",
     *     description="swipe direction",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Product_model")
     *    ),
     * 
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Discovery swipe failed"),
     * )
     */
     
    function swipe_get() {
        if(!$this->get('product_id') || !$this->get('direction'))
        {
           $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $product_id = $this->Discovery_model->swipe($this->get('product_id'), $this->get('direction'));
        
        $product = $this->Product_model->getProduct($product_id);
        $product = array_merge($product, array('access_token' => $access_token));

        if($product) {
            $this->response($product, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }


/**
     * @SWG\Get(path="/discovery/end",
     *   tags={"discovery"},
     *   summary="Discovery end",
     *   description="",
     *   operationId="discovery_end",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Discovery end"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     * )
     */
     
    function discovery_end_get() {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $this->Discovery_model->discovery_end();

        $this->response(NULL, 200); // 200 being the HTTP response code
    }
}
