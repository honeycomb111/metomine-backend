<h2><?php echo $title?></h2>
<p><a class="fright mbottom-20 btn btn-success" href="<?php echo site_url('/category/add');?>">Add</a></p>
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th class="width-5 text-center" scope="col">No</th>
        <th class="width-55 text-center" scope="col">Name</th>
        <th class="width-5 text-center" scope="col">Status</th>
        <th class="width-15 text-center" scope="col">Operate</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($categories) > 0):?>
    <?php foreach($categories as $category):?>
    <tr>
        <td class="text-center"><?php echo $category['ctg_id']?></td>
        <td><?php echo $category['ctg_name']?></td>
        <td class="text-center"><?php echo ($category['ctg_disabled'] == 0)?'Enable':'Disable'?></td>
        <td class="text-center">
            <a class="btn btn-success" href="<?php echo site_url('/category/'.$category['ctg_id']);?>">Edit</a>
            <a class="btn btn-success" href="<?php echo site_url('/category/remove/'.$category['ctg_id']);?>">Delete</a>
        </td>
    </tr>
    <?php endforeach;?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="4">There is no data</td>
        </tr>
    <?php endif;?>
    </tbody>
</table>
