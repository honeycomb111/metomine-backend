<?php
/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="metomine-honeycomb0814.c9users.io",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Swagger metomine",
 *         description="This is a sample server metomine.",
 *     ),
 * )
 */