<form>
    <input type="hidden" name="operate_type" value="<?php echo (isset($attribute_group['atg_id']) ? $attribute_group['atg_id'] : 0)?>">
    <fieldset class="margin-30">
        <div class="form-group">
            <label for="AttributeInputName">Name</label>
            <input type="text" class="form-control" id="AttributeInputName" name="atg_name" placeholder="Name" value="<?php echo (isset($attribute_group['atg_name'])?$attribute_group['atg_name']:'');?>">
        </div>
    </fieldset>
<!-------------Attribute--------->
<?php if(isset($attribute_group['atg_id'])):?>
    <p><a class="btn btn-success fright mbottom-20" href="<?php echo site_url('/attributes/add?atg_id='.$attribute_group['atg_id']);?>">Add Attribute</a></p>
    <table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th class="width-5 text-center" scope="col">No</th>
        <th class="width-55 text-center" scope="col">Name</th>
        <th class="width-15 text-center" scope="col">Operate</th>
    </tr>
    </thead>
    <tbody>
    <tbody>
        <?php if(count($attributes) > 0):?>
            <?php foreach($attributes as $attribute):?>
                <tr>
                    <td class="table_number text-center"><?php echo $attribute['atb_id']?></td>
                    <td class="table_name"><?php echo $attribute['atb_name']?></td>
                    <td class="text-center table_operate">
                        <a class="btn btn-success" href="<?php echo site_url('/attributes/'.$attribute['atb_id'].'?atg_id='.$attribute_group['atg_id']);?>">Edit</a>
                        <a class="btn btn-success" href="<?php echo site_url('/attributegroup/removeAttribute/'.$attribute['atb_id'].'/'.$attribute_group['atg_id']);?>">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
        <?php else:?>
        <tr><td colspan="3" align="center">There is no data.</td></tr>
        <?php endif;?>
    </tbody>
</table>
<?php endif;?>
<button type="submit" class="btn btn-success fright">Save</button>
<a class="btn btn-primary" href="<?php echo site_url('/attributegroup/');?>">Back</a>
</form>
<?php
if(isset($_GET['operate_type']) && isset($_GET['atg_name'])){
    $Attribute_model = new Attribute_model();
    $operate_type = $_GET['operate_type'];
    $attribute_data = array('atg_name'=>html_escape($_GET['atg_name']), 'atg_id'=>$_GET['atg_id']);
    if($operate_type == 0){
        $Attribute_model->addAttributeGroup($attribute_data);
    }else{
        $Attribute_model->updateAttributeGroup($operate_type, $attribute_data);
    }
    redirect('/attributegroup');
}
?>

