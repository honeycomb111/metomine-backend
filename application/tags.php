<?php
/**
 * @SWG\Tag(
 *   name="user",
 *   description="About user",
 * )
 * @SWG\Tag(
 *   name="product",
 *   description="About Products",
 * )
 * @SWG\Tag(
 *   name="category",
 *   description="About Categories"
 * )
 * @SWG\Tag(
 *   name="attribute",
 *   description="About attributes",
 * )
 * @SWG\Tag(
 *   name="discovery",
 *   description="About discovery",
 * )
 */