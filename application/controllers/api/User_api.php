<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Tensai
 * @license         CS
 */
class User_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('session');
        $this->load->model('User_model');
    }

    /**
     * @SWG\Get(path="/user/get",
     *   tags={"user"},
     *   summary="Get a user",
     *   description="",
     *   operationId="user_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="user_id",
     *     description="The user id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/User_model")
     *    ),
     * 
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get a user failed"),
     * )
     */
    function user_get()
    {
        if(!$this->get('user_id'))
        {
           $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $user = $this->User_model->getUser( $this->get('user_id') );
        
        if (isset($user['user_password'])) unset($user['user_password']);
        if (isset($user['facebook_uid'])) unset($user['facebook_uid']);
                
        $user = array_merge($user, array('access_token' => $access_token));
        if($user) {
            $this->response($user, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }

/**
     * @SWG\Post(path="/user/login",
     *   tags={"user"},
     *   summary="Logs user into the system",
     *   description="",
     *   operationId="LoginUser",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Created user object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/User_login_model")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/User_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=404, description="Invalid username/password supplied"),
     * )
     */
     
    function login_post()
    {
        if($this->post('type') == 'fb') {
            // facebook log in
            if (!$this->post('user_email') || !$this->post('facebook_uid')) {
                $this->response(NULL, 400);
            }
            
            $user = $this->User_model->existUserFromFb($this->post('user_email'), $this->post('facebook_uid'));
        
            if($user)
            {
                $this->session->set_userdata('user_id' , $user['user_id']);
    
                $token = md5(uniqid(mt_rand(), true));
                $this->session->set_userdata('access_token' , $token);
    
                if (isset($user['user_password'])) unset($user['user_password']);
                if (isset($user['facebook_uid'])) unset($user['facebook_uid']);
                
                $user = array_merge($user, array('access_token' => $token));
                $this->response($user, 200); // 200 being the HTTP response code
            }
            else
            {
                $this->response(NULL, 404);
            }
            
        } else if($this->post('type') == 'email') {
            // email log in
            if (!$this->post('user_email') || !$this->post('user_password')) {
                $this->response(NULL, 400);
            }
            
            $user = $this->User_model->existUser($this->post('user_email'), $this->post('user_password'));
        
            if($user)
            {
                $this->session->set_userdata('user_id' , ($user['user_id']));
    
                $token = md5(uniqid(mt_rand(), true));
                $this->session->set_userdata('access_token' , $token);
    
                if (isset($user['user_password'])) unset($user['user_password']);
                if (isset($user['facebook_uid'])) unset($user['facebook_uid']);
                
                $user = array_merge($user, array('access_token' => $token));
                $this->response($user, 200); // 200 being the HTTP response code
            }
            else
            {
                $this->response(NULL, 404);
            }
        
        } else {
           $this->response(NULL, 400); 
        }
    }
    
    /**
     * @SWG\Get(path="/user/logout",
     *   tags={"user"},
     *   summary="Log out user system",
     *   description="",
     *   operationId="logoutUser",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation"),
     * 
     *   @SWG\Response(response=401, description="Token mismatch"),
     * )
     */
     
    function logout_get()
    {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('access_token');

        $unset_success = 1;
        $this->response(NULL, 200); // 200 being the HTTP response code

    }
    
    /**
     * @SWG\Post(path="/user/signup",
     *   tags={"user"},
     *   summary="Create a user",
     *   description="",
     *   operationId="createUser",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Created user object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/User_model1")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/User_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=404, description="Create a user failed"),
     * )
     */
    function user_post()
    {
        if($this->post('type') == 'fb') {
            // facebook sign up
            
            if (!$this->post('user_name') || !$this->post('user_email') || !$this->post('facebook_uid')) {
                $this->response(NULL, 400);
            }
            
            $user = $this->User_model->getUserByEmail($this->post('user_email'));
            
            if (!$user) {
                $user = array(
                    'user_name' => $this->post('user_name'),
                    'user_email' => $this->post('user_email'),
                    'facebook_uid' => $this->post('facebook_uid'),
                );
                $user_id = (string)$this->User_model->addUserFromFB($user);    
                
                $user = array_merge($user, array('user_id' => $user_id));
            } else {
                $user_id = $user['user_id'];
            }
            
            if ($user_id) {
                $this->session->set_userdata('user_id', $user_id);

                $token = md5(uniqid(mt_rand(), true));
                $this->session->set_userdata('access_token' , $token);

                if (isset($user['user_password'])) unset($user['user_password']);
                if (isset($user['facebook_uid'])) unset($user['facebook_uid']);
                
                $user = array_merge((array)$user, array('access_token' => $token));
                $this->response($user, 200); // 200 being the HTTP response code

            } else {
            	$this->response(NULL, 404);
            }
            
        } else if($this->post('type') == 'email') {
            // email sign up
            if (!$this->post('user_name') || !$this->post('user_email') || !$this->post('user_password')) {
                $this->response(NULL, 400);
            }
            $user = $this->User_model->getUserByEmail($this->post('user_email'));
            
            if (!$user) {
                $user = array(
                    'user_name' => $this->post('user_name'),
                    'user_email' => $this->post('user_email'),
                    'user_password' => $this->post('user_password'),
                );
        
                $user_id = (string)$this->User_model->addUser($user);
                $user = array_merge($user, array('user_id' => $user_id));
            } else {
                $user_id = $user['user_id'];
            }
            
            if ($user_id) {
                $this->session->set_userdata('user_id', $user_id);
    
                $token = md5(uniqid(mt_rand(), true));
                $this->session->set_userdata('access_token' , $token);
    
                if (isset($user['user_password'])) unset($user['user_password']);
                if (isset($user['facebook_uid'])) unset($user['facebook_uid']);
                
                $user = array_merge($user, array('access_token' => $token));
                $this->response($user, 200); // 200 being the HTTP response code
    
            } else {
                $this->response(NULL, 404);
            }
        } else {
           $this->response(NULL, 400); 
        }
    }
    
    /**
     * @SWG\Put(path="/user/modify",
     *   tags={"user"},
     *   summary="Update a user",
     *   description="",
     *   operationId="updateUser",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Updated user object",
     *     required=false,
     *     @SWG\Schema(ref="#/definitions/User_update_model")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/User_model")
     *    ),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Update a user failed"),
     * )
     */
     
    function user_put()
    {
        // if(!$this->put('user_name') || !$this->put('user_email') || !$this->put('user_password'))
        // {
        //   $this->response(NULL, 400);
        // }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $user = array(
            'user_name' => $this->put('user_name'),
            'user_email' => $this->put('user_email'),
            'user_password' => $this->put('user_password'),
        );


        $user_id = $this->session->userdata('user_id');

        if ($this->User_model->updateUser($user_id, $user)) {

            $user= array_merge($user, array('user_id' => $user_id, 'access_token' => $access_token));
            $this->response($user, 200); // 200 being the HTTP response code
        } else {
            $this->response(NULL, 404);
        }
    }
    


}
