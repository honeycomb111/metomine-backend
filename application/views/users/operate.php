<form>
    <input type="hidden" name="operate_type" value="<?php echo (isset($user['user_id']) ? $user['user_id'] : 0)?>">
    <fieldset class="margin-30">
        <div class="form-group">
            <label for="UserInputName">Name</label>
            <input type="text" class="form-control" id="UserInputName" name="user_name" placeholder="Name" value="<?php echo (isset($user['user_name'])?$user['user_name']:'');?>">
        </div>
        <div class="form-group">
            <label for="UserInputEmail1">Email address</label>
            <input type="email" class="form-control" id="UserInputEmail1" name="user_email" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo (isset($user['user_email'])?$user['user_email']:'');?>">
        </div>
        <div class="form-group">
            <label for="UserInputPassword1">Password</label>
            <input type="password" class="form-control" id="UserInputPassword1" name="user_password" placeholder="Password" value="<?php echo (isset($user['user_password'])?$user['user_password']:'');?>">
        </div>
        <a class="btn btn-primary" href="<?php echo site_url('/users/');?>">Back</a>
        <button type="submit" class="btn btn-success fright">Save</button>
    </fieldset>
</form>
<?php
    if(isset($_GET['operate_type']) && isset($_GET['user_name']) && isset($_GET['user_email']) && isset($_GET['user_password'])){
        $User_model = new User_model();
        $operate_type = $_GET['operate_type'];
        $user_data = array('user_name'=>html_escape($_GET['user_name']), 'user_email'=>$_GET['user_email'], 'user_password'=>$_GET['user_password']);
        if($operate_type == 0){
            $User_model->addUser($user_data);
        }else{
            $User_model->updateUser($operate_type, $user_data);
        }
        redirect('/users');
    }
?>