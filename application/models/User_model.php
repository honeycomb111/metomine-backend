<?php

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="User"))
 */
class User_model extends CI_Model {
    /**
     * @SWG\Property()
     * @var string
     */
    public $user_id;
    /**
     * @SWG\Property()
     * @var string
     */
    public $user_name;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_email;
    /**
     * @var string
     * @SWG\Property()
     */
    public $access_token;

    
    public function getAllUser($user_id = 0)
    {
        if($user_id > 0){
            $sql = "SELECT * FROM user WHERE user_id = {$user_id} ";
        }else{
            $sql = "SELECT * FROM user WHERE user_id > 0 ";
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return ($user_id > 0)?$result[0]:$result;
    }

    public function checkFbUid($fbUid){
        $sql = "SELECT * FROM user WHERE facebook_uid='{$fbUid}'";
        $result = $this->db->query($sql)->result_array();
        
        return isset($result[0]) ? $result[0] : null;
        
    }

    public function getUser($user_id) 
    {
        $sql = "SELECT * FROM user WHERE user_id={$user_id}";
        $result = $this->db->query($sql)->result_array();
        
        return isset($result[0]) ? $result[0] : null;
    }

    public function getUserByEmail($user_email) 
    {
        $sql = "SELECT * FROM user WHERE user_email='{$user_email}'";
        $result = $this->db->query($sql)->result_array();
        
        return isset($result[0]) ? $result[0] : null;
    }


    public function existUser($user_email, $user_password)
    {
        $user_password = md5($user_password);
        $sql = "SELECT * FROM user WHERE user_email='{$user_email}' AND user_password='{$user_password}'";
        $result = $this->db->query($sql)->result_array();
        
        return isset($result[0]) ? $result[0] : null;
    }

    public function existUserFromFb($user_email, $facebook_uid)
    {
        $sql = "SELECT * FROM user WHERE user_email='{$user_email}' AND facebook_uid='{$facebook_uid}'";
        $result = $this->db->query($sql)->result_array();
        
        return isset($result[0]) ? $result[0] : null;
    }

    public function addUser($user_data) 
    {
        $user_password = md5($user_data['user_password']);
        $sql = "INSERT INTO user (user_name, user_email, user_password) VALUES ('{$user_data['user_name']}', '{$user_data['user_email']}', '{$user_password}')";
        $this->db->query($sql);
        return $this->db->insert_id();
    }
    public function addUserFromFB($user_data){
        $sql = "INSERT INTO user (user_name, user_email, facebook_uid) VALUES ('{$user_data['user_name']}', '{$user_data['user_email']}', '{$user_data['facebook_uid']}')";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function updateUser($user_id, $user_data) 
    {
        $user_password = md5($user_data['user_password']);
        $sql = "UPDATE user SET user_name='{$user_data['user_name']}', user_email='{$user_data['user_email']}', user_password='{$user_password}' WHERE user_id={$user_id}";
        return $this->db->query($sql);
    }

    public function deleteUser($user_id) 
    {
        $sql = "DELETE FROM user WHERE user_id={$user_id}";
        $this->db->query($sql);
    }

}

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="User"))
 */
class User_model1{
    /**
     * @SWG\Property()
     * @var string
     */
    public $user_name;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_email;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_password;
    /**
     * @var string
     * @SWG\Property(example="fb")
     */
    public $type;
    /**
     * @var string
     * @SWG\Property()
     */
    public $facebook_uid;
}


/**
 * @SWG\Definition(type="object", @SWG\Xml(name="User"))
 */
class User_login_model{
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_email;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_password;
    /**
     * @var string
     * @SWG\Property(example="fb")
     */
    public $type;
    /**
     * @var string
     * @SWG\Property()
     */
    public $facebook_uid;
    
}

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="User"))
 */
class User_update_model{
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_name;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_email;
    /**
     * @var string
     * @SWG\Property()
     */
    public $user_password;
}

