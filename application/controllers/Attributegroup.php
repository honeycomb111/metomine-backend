<?php
class Attributegroup extends CI_Controller {
	public function test(){
		var_dump("test");
	}
	public function index(){
		$data['title'] = 'AttributeGroup';
		$data['attribute_group'] = $this->Attribute_model->getAttributeGroups();
		$this->load->view('templates/header');
		$this->load->view('attributegroup/index', $data);
		$this->load->view('templates/footer');
	}

	public function operate($id){
		$data = array();
		$data['attributes'] = $this->Attribute_model->getAllAttributes(0, $id);
		if($id != 'add'){
			$data['attribute_group'] = $this->Attribute_model->getAttributeGroup($id);
			if(empty($data['attribute_group'])){
				show_404();
			}
			$data['atg_id'] = $data['attribute_group']['atg_id'];
			$data['atg_name'] = $data['attribute_group']['atg_name'];
		}
		$this->load->view('templates/header');
		$this->load->view('attributegroup/operate', $data);
		$this->load->view('templates/footer');
	}
	public function remove($id){
		$this->Attribute_model->deleteAttributeGroup($id);
		redirect('/attributegroup');
	}
	public function removeAttribute($id, $atg_id){
		$this->Attribute_model->deleteAttribute($id);
		redirect('/attributegroup/'.$atg_id);
	}
}
