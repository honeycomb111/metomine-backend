<?php
class Attributes extends CI_Controller {

	public function index(){
		$data['title'] = 'Attribute';
		$data['attributes'] = $this->Attribute_model->getAllAttributes();
		$data['attribute_group'] = $this->Attribute_model->getAttributeGroups();
		$this->load->view('templates/header');
		$this->load->view('attributes/index', $data);
		$this->load->view('templates/footer');
	}

	public function operate($id){
		$data = array();
		$data['attribute_group'] = $this->Attribute_model->getAttributeGroups();
		$data['attribute'] = array();
		if($id != 'add'){
			$data['attribute'] = $this->Attribute_model->getAllAttributes($id);
			if(empty($data['attribute'])){
				show_404();
			}
			$data['atb_id'] = $data['attribute']['atb_id'];
			$data['atb_name'] = $data['attribute']['atb_name'];
		}
		$this->load->view('templates/header');
		$this->load->view('attributes/operate', $data);
		$this->load->view('templates/footer');
	}
	public function remove($id){
		$this->Attribute_model->deleteAttribute($id);
		redirect('/attributes');
	}
}
