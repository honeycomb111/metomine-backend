<h2><?php echo $title?></h2>
<p><a class="btn btn-success fright mbottom-20" href="<?php echo site_url('/users/add');?>">Add</a></p>
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th class="width-5 text-center" scope="col">No</th>
        <th class="width-35 text-center" scope="col">Name</th>
        <th class="width-35 text-center" scope="col">Email</th>
        <th class="width-15 text-center" scope="col">Operate</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($users) > 0):?>
    <?php foreach($users as $user):?>
    <tr>
        <td align="center"><?php echo $user['user_id']?></td>
        <td><?php echo $user['user_name']?></td>
        <td><?php echo $user['user_email']?></td>
        <td class="text-center">
            <a class="btn btn-success" href="<?php echo site_url('/users/'.$user['user_id']);?>">Edit</a>
            <a class="btn btn-success" href="<?php echo site_url('/users/remove/'.$user['user_id']);?>">Delete</a>
        </td>
    </tr>
    <?php endforeach;?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="4">There is no data</td>
        </tr>
    <?php endif;?>
    </tbody>
</table>
