<?php
class Category extends CI_Controller {

	public function index(){
		$data['title'] = 'Category';
		$data['categories'] = $this->Category_model->getAllCategory();
		$this->load->view('templates/header');
		$this->load->view('category/index', $data);
		$this->load->view('templates/footer');
	}

	public function operate($id){
		$data = array();
		if($id != 'add'){
			$data['category'] = $this->Category_model->getAllCategory($id);
			if(empty($data['category'])){
				show_404();
			}
			$data['ctg_id'] = $data['category']['ctg_id'];
			$data['ctg_name'] = $data['category']['ctg_name'];
			$data['ctg_disabled'] = $data['category']['ctg_disabled'];
		}
		$this->load->view('templates/header');
		$this->load->view('category/operate', $data);
		$this->load->view('templates/footer');
	}
	public function remove($id){
		$this->Category_model->deleteCategory($id);
		redirect('/category');
	}
}
