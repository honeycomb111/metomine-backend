<?php
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Attribute"))
 */
 
class Attribute_model extends CI_Model {
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $atg_id;
    /**
     * @SWG\Property()
     * @var string
     */
    public $atg_name;

    public function getAttributeGroups() {
        $sql = "SELECT * FROM attribute_group WHERE 1";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAttributeGroup($atg_id) {   
        $sql = "SELECT * FROM attribute_group WHERE atg_id={$atg_id}";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0])? $result[0] : null;
    }

    public function getAllAttributes($attribute_id = 0, $attribute_group_id = 0) {
        if($attribute_id > 0){
            $sql = "SELECT * FROM attribute WHERE atb_id = {$attribute_id} ";
        }else{
            if($attribute_group_id > 0){
                $sql = "SELECT * FROM attribute WHERE atb_id > 0 AND atb_atg_id = {$attribute_group_id}";
            }else{
//                $sql = "SELECT * FROM `attribute` LEFT JOIN attribute_group ON attribute.atb_atg_id = attribute_group.atg_id WHERE atb_id > 0 ";
                $sql = "SELECT * FROM attribute WHERE atb_id > 0 ";
            }
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return ($attribute_id > 0)?$result[0]:$result;
    }

    public function getAttributes($atg_id) {
        $sql = "SELECT * FROM attribute WHERE atb_atg_id={$atg_id}";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAttribute($atb_id) {   
        $sql = "SELECT * FROM attribute WHERE atb_id={$atb_id}";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0])? $result[0] : null;
    }

    public function addAttributeGroup($atg_data) {
        $sql = "INSERT INTO attribute_group (atg_name) VALUES ('{$atg_data['atg_name']}')";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function addAttribute($atb_data) {
        $sql = "INSERT INTO attribute (atb_name, atb_atg_id) VALUES ('{$atb_data['atb_name']}', {$atb_data['atb_atg_id']})";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function updateAttributeGroup($atg_id, $atg_data) {
        $sql = "UPDATE attribute_group SET atg_name='{$atg_data['atg_name']}' WHERE atg_id={$atg_id}";
        return $this->db->query($sql);
    }

    public function updateAttribute($atb_id, $atb_data) {
        $sql = "UPDATE attribute SET atb_name='{$atb_data['atb_name']}', atb_atg_id = {$atb_data['atb_atg_id']} WHERE atb_id={$atb_id}";
        return $this->db->query($sql);
    }


    public function deleteAttributeGroup($atg_id) {
        $sql = "DELETE FROM attribute_group WHERE atg_id = {$atg_id}";
        $result = $this->db->query($sql);

        $sql = "DELETE FROM attribute WHERE atb_atg_id = {$atg_id}";
        return ($result && $this->db->query($sql));
    }

    public function deleteAttribute($atb_id) {
        $sql = "DELETE FROM attribute WHERE atb_id = {$atb_id}";
        return $this->db->query($sql);
    }

}

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="Value"))
 */
 
class Value_model {
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $atb_id;
    /**
     * @SWG\Property()
     * @var string
     */
    public $atb_name;
}