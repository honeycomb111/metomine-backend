   <head> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      
      <script src="<?php echo base_url();?>/assets/js/jquery.ajaxfileupload.js"></script>
      
   </head>
   
<form>
    <input type="hidden" name="operate_type" value="<?php echo (isset($product['pdt_id']) ? $product['pdt_id'] : 0)?>">
    <fieldset class="margin-30">
        <div class="form-group">
            <label for="ProductInputName">Name</label>
            <input type="text" class="form-control" id="ProductInputName" name="pdt_name" placeholder="Name" value="<?php echo (isset($product['pdt_name'])?$product['pdt_name']:'');?>">
        </div>
        <div class="form-group">
            <label for="exampleTextarea">Product Description</label>
            <textarea name="pdt_description" class="form-control" id="exampleTextarea" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 94px;"><?php echo (isset($product['pdt_description'])?$product['pdt_description']:'');?></textarea>
        </div>
        <div class="form-group">
            <label for="ProductInputPrice">Price</label>
            <input type="text" class="form-control" id="ProductInputPrice" name="pdt_price" placeholder="Price" value="<?php echo (isset($product['pdt_price'])?$product['pdt_price']:'');?>">
        </div>
        <div class="form-group">
            <label for="ProductCategorySelect">Category</label>
            <select class="form-control" name="ctg_id" id="ProductCategorySelect">
                <?php foreach($category as $key=>$value):?>
                    <?php if(isset($product['ctg_id']) && $product['ctg_id'] > 0):?>
                        <option <?php echo (($product['ctg_id'] == $value['ctg_id']) ? 'selected' : '')?> value="<?php echo $value['ctg_id']?>"><?php echo $value['ctg_name']?></option>
                    <?php else: ?>
                        <option value="<?php echo $value['ctg_id']?>"><?php echo $value['ctg_name']?></option>
                    <?php endif;?>
                <?php endforeach;?>
            </select>
        </div>
        <label for="ProductCategorySelect">Attribute</label>
        <?php
        ?>
        <table class="table table-hover table-bordered">
            <tbody>
            <?php foreach($attribute_data as $key=>$value):?>
                <tr>
                    <td><?php echo $value['atg_name']?></td>
                    <td>
                        <select class="form-control fright" name="atb_id[]" id="AttributeSelect">
                            <?php foreach($value['attribute'] as $key1=>$value1):?>
                                <?php if($product['pdt_id'] > 0):?>
                                    <option value="<?php echo $value1['atb_id']?>" <?php
                                    foreach($product_to_attribute as $key2=>$value2) {
                                        echo(($value2['pta_attribute_id'] == $value1['atb_id']) && ($value2['pta_id'] == $product['pdt_id']) ? 'selected' : '');
                                    }
                                    ?>>
                                    <?php echo $value1['atb_name']?>
                                    </option>
                                    <?php else:?>
                                    <option value="<?php echo $value1['atb_id']?>" >
                                        <?php echo $value1['atb_name']?>
                                    </option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        
        
        <input type="file" id="one-specific-file" name="one-specific-file">
        

        <a class="btn btn-primary" href="<?php echo site_url('/products/');?>">Back</a>
        <button type="submit" class="btn btn-success fright">Save</button>
    </fieldset>
</form>

<script type="text/javascript">
/*global $*/
$('#one-specific-file').ajaxfileupload({
  action: '/ajax-image-upload/post',
  
  params: {
    extra: 'info'
  },
  onComplete: function(response) {
    console.log('custom handler for file:');
    alert(JSON.stringify(response));
  },
  onStart: function() {
    // if(weWantedTo) return false; // cancels upload
  },
  onCancel: function() {
    console.log('no file selected');
  }
});


     

</script>

<?php
    if(isset($_GET['operate_type']) && isset($_GET['pdt_name']) && isset($_GET['pdt_description']) && isset($_GET['pdt_price']) && isset($_GET['ctg_id']) && isset($_GET['atb_id'])){
        $Product_model = new Product_model();
        $operate_type = $_GET['operate_type'];
        $tmp_arr_attribute = $_GET['atb_id'];
        $product_data = array('pdt_name'=>html_escape($_GET['pdt_name']), 'pdt_description'=>html_escape($_GET['pdt_description']),'pdt_price' => $_GET['pdt_price'],'category'=>$_GET['ctg_id'], 'attributes' => array_filter($tmp_arr_attribute));
        if($operate_type == 0){
            $Product_model->addProduct($product_data);
        }else{
            $Product_model->updateProduct($operate_type, $product_data);
        }
        redirect('/products');
    }
?>