<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Tensai
 * @license         CS
 */
class category_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Category_model');
    }

    /**
     * @SWG\Get(path="/categories/get",
     *   tags={"category"},
     *   summary="Get All Categories",
     *   description="",
     *   operationId="categories_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *        type="array",
     *        @SWG\Items(ref="#/definitions/Category_model")
     *     ),
     *    ),
     *   @SWG\Response(response=401, description="Token Mismatch"),
     *   @SWG\Response(response=404, description="Get categories failed"),
     * )
     */
    function categories_get() 
    {
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $categories = $this->Category_model->getCategories();
        $categories = array_merge($categories, array('access_token' => $access_token));

        if($categories)
        {
            $this->response($categories, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }        
    }

    /**
     * @SWG\Get(path="/category/get",
     *   tags={"category"},
     *   summary="Get a Category by given id",
     *   description="",
     *   operationId="category_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="category_id",
     *     description="The category id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="access_token",
     *     description="The access token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *       ref="#/definitions/Category_model")
     *    ),
     *   @SWG\Response(response=400, description="Missing required params"),
     *   @SWG\Response(response=401, description="Token mismatch"),
     *   @SWG\Response(response=404, description="Get a category failed"),
     * )
     */
    function category_get()
    {
        if(!$this->get('category_id')) {
            $this->response(NULL, 400);
        }
        
        $access_token = $this->session->userdata('access_token');
        if (!$access_token || $this->get('access_token') != $access_token) {
            $this->response(NULL, 401);
        }

        $category = $this->Category_model->getCategory($this->get('category_id'));
        $category = array_merge($category, array('access_token' => $access_token));

        if($category) {
            $this->response($category, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(NULL, 404);
        }
    }
}
