<form>
    <input type="hidden" name="operate_type" value="<?php echo (isset($attribute['atb_id']) ? $attribute['atb_id'] : 0)?>">
    <fieldset class="margin-30">
        <div class="form-group">
            <input type="hidden" name="atb_atg_id" value="<?php echo $_GET['atg_id']?>">
        </div>
        <div class="form-group">
            <label for="AttributeInputName">Name</label>
            <input type="text" class="form-control" id="AttributeInputName" name="atb_name" placeholder="Name" value="<?php echo (isset($attribute['atb_name'])?$attribute['atb_name']:'');?>">
        </div>
        <a class="btn btn-primary" href="<?php echo site_url('/attributegroup/'.$_GET['atg_id']);?>">Back</a>
        <button type="submit" class="btn btn-success fright">Save</button>
    </fieldset>
</form>
<?php
    if(isset($_GET['operate_type']) && isset($_GET['atb_name']) && isset($_GET['atb_atg_id'])){
        $Attribute_model = new Attribute_model();
        $operate_type = $_GET['operate_type'];
        $attribute_data = array('atb_name'=>html_escape($_GET['atb_name']), 'atb_atg_id'=>$_GET['atb_atg_id']);
        if($operate_type == 0){
            $Attribute_model->addAttribute($attribute_data);
        }else{
            $Attribute_model->updateAttribute($operate_type, $attribute_data);
        }
        redirect('/attributegroup/'.$_GET['atb_atg_id']);
    }
?>