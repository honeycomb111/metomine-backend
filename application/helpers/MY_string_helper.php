<?php

if ( ! function_exists('sql_where')) {
    function sql_where($sql, $condition) {
        if (!strpos($sql, "WHERE")){
            $sql .= " WHERE " . $condition;
        } else {
            $sql .= " AND " . $condition;
        }

        return $sql;
    }
}

?>
